package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existsById(String id);

    Project remove(Project project);

    void clear();

    Project removeById(String id);

    int getSize();

}
