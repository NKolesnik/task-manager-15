package ru.t1consulting.nkolesnik.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void showSystemInfo();

    void close();

}

