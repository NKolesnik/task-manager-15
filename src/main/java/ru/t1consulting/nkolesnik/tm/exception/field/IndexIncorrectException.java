package ru.t1consulting.nkolesnik.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException{

    public IndexIncorrectException() {
        super("Error! Incorrect index...");
    }

}
